drop schema if exists accountms;

create schema accountms;

use accountms;

CREATE TABLE bank_account (
  id int NOT NULL,
  acc_num bigint DEFAULT NULL,
  bank_name varchar(100) DEFAULT NULL,
  ifsc_code varchar(100) DEFAULT NULL,
  branch varchar(100) DEFAULT NULL,
  acc_holder_name varchar(15) DEFAULT NULL,
  address varchar(1000) DEFAULT NULL,
  created_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE client (
  id int NOT NULL,
  name varchar(100) DEFAULT NULL,
  code varchar(50) DEFAULT NULL,
  created_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE company (
  id int NOT NULL,
  name varchar(100) DEFAULT NULL,
  code varchar(50) DEFAULT NULL,
  created_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE expense_category (
  id int NOT NULL,
  category_name varchar(50) DEFAULT NULL,
  category_code varchar(50) DEFAULT NULL,
  created_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE invoice (
  id int NOT NULL AUTO_INCREMENT,
  company_id int DEFAULT NULL,
  client_id int DEFAULT NULL,
  bank_account_id int DEFAULT NULL,
  invoice_number varchar(50) NOT NULL,
  date date DEFAULT NULL,
  amount int DEFAULT NULL,
  status varchar(15) DEFAULT NULL,
  cgst_percent int DEFAULT NULL,
  sgst_percent int DEFAULT NULL,
  expected_credit_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  created_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY invoice_number_UNIQUE (invoice_number),
  KEY fk_inv_cmpy_id_idx (company_id),
  KEY fk_inv_client_id_idx (client_id),
  KEY fk_bank_acc_id_idx (bank_account_id),
  CONSTRAINT fk_inv_bank_acc_id FOREIGN KEY (bank_account_id) REFERENCES bank_account (id),
  CONSTRAINT fk_inv_client_id FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT fk_inv_cmpy_id FOREIGN KEY (company_id) REFERENCES company (id)
);

CREATE TABLE expense (
  id int NOT NULL AUTO_INCREMENT,
  company_id int DEFAULT NULL,
  expense_category_id int DEFAULT NULL,
  bank_account_id int DEFAULT NULL,
  expense_type varchar(100) DEFAULT NULL,
  amount int DEFAULT NULL,
  cgst_percent int DEFAULT '0',
  sgst_percent int DEFAULT '0',
  status varchar(15) DEFAULT NULL,
  debit_date date DEFAULT NULL,
  created_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_cmpy_id_idx (company_id),
  KEY fk_exp_cat_id_idx (expense_category_id),
  KEY fk_bank_acc_id_idx (bank_account_id),
  CONSTRAINT fk_bank_acc_id FOREIGN KEY (bank_account_id) REFERENCES bank_account (id),
  CONSTRAINT fk_cmpy_id FOREIGN KEY (company_id) REFERENCES company (id),
  CONSTRAINT fk_exp_cat_id FOREIGN KEY (expense_category_id) REFERENCES expense_category (id)
);

CREATE TABLE income (
  id int NOT NULL AUTO_INCREMENT,
  company_id int DEFAULT NULL,
  client_id int DEFAULT NULL,
  amount bigint DEFAULT NULL,
  invoice_id int DEFAULT NULL,
  cgst_percent int DEFAULT NULL,
  sgst_percent int DEFAULT NULL,
  credit_date datetime DEFAULT NULL,
  bank_account_id int DEFAULT NULL,
  created_date date DEFAULT NULL,
  created_by varchar(50) DEFAULT NULL,
  modified_date date DEFAULT NULL,
  modified_by varchar(50) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_cmpy_id_idx (company_id),
  KEY fk_client_id_idx (client_id),
  KEY fk_bank_acc_id_idx (bank_account_id),
  KEY fk_inc_inv_id_idx (invoice_id),
  CONSTRAINT fk_inc_bank_acc_id FOREIGN KEY (bank_account_id) REFERENCES bank_account (id),
  CONSTRAINT fk_inc_client_id FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT fk_inc_cmpy_id FOREIGN KEY (company_id) REFERENCES company (id),
  CONSTRAINT fk_inc_inv_id FOREIGN KEY (invoice_id) REFERENCES invoice (id)
);
