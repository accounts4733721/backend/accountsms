-- create company
insert into accountms.company (id, name, code, created_date, created_by, modified_date, modified_by) values 
(1, 'ADVENTO TECHNOLOGIES PRIVATE LIMITED', 'ADVENTO', sysdate(), 'SYSTEM', sysdate(), 'SYSTEM'),
(2, 'BWINS TECHNOLOGIES PRIVATE LIMITED', 'BWINS', sysdate(), 'SYSTEM', sysdate(), 'SYSTEM');

-- create client
insert into accountms.client (id, name, code, created_date, created_by, modified_date, modified_by) values 
(1, 'BRILLIO TECHNOLOGIES PRIVATE LIMITED', 'ADVENTO', sysdate(), 'SYSTEM', sysdate(), 'SYSTEM'),
(2, 'PRODAPT TECHNOLOGIES PRIVATE LIMITED', 'BWINS', sysdate(), 'SYSTEM', sysdate(), 'SYSTEM');


-- create expense_category
insert into accountms.expense_category values (1, 'mess', 'mess', sysdate(), 'System', sysdate(), 'System');

-- create bank_account
INSERT INTO accountms.bank_account (id, acc_num, bank_name, ifsc_code, branch, acc_holder_name, address, created_date, created_by, modified_date, modified_by) VALUES ('1', '789078', 'HDFC BANK LTD', 'HDFC000441', 'NELSON MANICKAM', 'RK', 'UGI', sysdate(), 'SYSTEM', sysdate(), 'SYSTEM');
