package com.adv.accountms.util;

import com.adv.accountms.dto.BankAccountDto;
import com.adv.accountms.dto.ClientDto;
import com.adv.accountms.dto.CompanyDto;
import com.adv.accountms.dto.ExpenseCategoryDto;
import com.adv.accountms.dto.InvoiceDto;
import com.adv.accountms.dto.TableMaintenanceDto;
import com.adv.accountms.entity.BankAccountEntity;
import com.adv.accountms.entity.ClientEntity;
import com.adv.accountms.entity.CompanyEntity;
import com.adv.accountms.entity.ExpenseCategoryEntity;
import com.adv.accountms.entity.InvoiceEntity;
import com.adv.accountms.entity.TableMaintenanceEntity;

public class EntityToDtoUtil {

	public static ExpenseCategoryDto transformEntityToDto(ExpenseCategoryEntity expenseCategoryEntity) {
		return new ExpenseCategoryDto(expenseCategoryEntity.getId(), expenseCategoryEntity.getCategoryName(),
				expenseCategoryEntity.getCategoryCode());
	}

	public static CompanyDto transformEntityToDto(CompanyEntity companyEntity) {
		return new CompanyDto(companyEntity.getId(), companyEntity.getName(), companyEntity.getCode());
	}
	
	public static ClientDto transformEntityToDto(ClientEntity clientEntity) {
		return new ClientDto(clientEntity.getId(), clientEntity.getName(), clientEntity.getCode());
	}

	public static BankAccountDto transformEntityToDto(BankAccountEntity bankAccountEntity) {
		return new BankAccountDto(bankAccountEntity.getId(), bankAccountEntity.getAccNum(),
				bankAccountEntity.getBankName(), bankAccountEntity.getIfscCode(), bankAccountEntity.getBranch(),
				bankAccountEntity.getAccHolderName(), bankAccountEntity.getAddress());
	}
	
	public static InvoiceDto transformEntityToDto(InvoiceEntity invoiceEntity) {
		return new InvoiceDto(invoiceEntity.getId(), transformEntityToDto(invoiceEntity.getCompanyEntity()),
				transformEntityToDto(invoiceEntity.getClientEntity()),
				transformEntityToDto(invoiceEntity.getBankAccountEntity()), invoiceEntity.getInvoiceNumber(),
				invoiceEntity.getAmount(), invoiceEntity.getDate(), invoiceEntity.getStatus(),
				invoiceEntity.getCgstPercent(), invoiceEntity.getSgstPercent(), invoiceEntity.getExpectedCreditDate());
	}
	
	public static void setTableMaintainceDetails(TableMaintenanceDto outputDto, TableMaintenanceEntity inputEntity ) {
		outputDto.setCreatedBy(inputEntity.getCreatedBy());
		outputDto.setCreatedDate(inputEntity.getCreatedDate());
		outputDto.setModifiedBy(inputEntity.getModifiedBy());
		outputDto.setModifiedDate(inputEntity.getModifiedDate());
	}
}
