package com.adv.accountms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.client.RestTemplate;

import com.adv.accountms.auditaware.AuditorAwareImpl;

@SpringBootApplication
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class AccountmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountmsApplication.class, args);
	}
	
	@Bean
	public AuditorAware<String> auditorProvider() {
		return new AuditorAwareImpl();
	}
	
	@Bean
	public RestTemplate createRestTemplate() {
		return new RestTemplate();
	}
}
