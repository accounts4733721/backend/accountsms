package com.adv.accountms.entity;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity (name = "expense")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ExpenseEntity extends TableMaintenanceEntity {

	@Id
    //@GeneratedValue(strategy = GenerationType.UUID)
	//private UUID id;
	@GeneratedValue(strategy= GenerationType.AUTO,generator="native")
	@GenericGenerator(name = "native",strategy = "native")
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private CompanyEntity companyEntity;
	
	@ManyToOne
	@JoinColumn(name = "expense_category_id")
	private ExpenseCategoryEntity expenseCategoryEntity;
	
	@ManyToOne
	@JoinColumn(name = "bank_account_id")
	private BankAccountEntity bankAccountEntity;
	
	private String expenseType;
	
	private long amount;
	
	private String status;
	
	//@Temporal(TemporalType.TIMESTAMP)
	private String debitDate;

	private int cgstPercent;
	
	private int sgstPercent;
}
