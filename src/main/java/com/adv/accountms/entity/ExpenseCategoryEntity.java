package com.adv.accountms.entity;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity (name = "expense_category")
@Data
@NoArgsConstructor
public class ExpenseCategoryEntity extends TableMaintenanceEntity {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO,generator="native")
	@GenericGenerator(name = "native",strategy = "native")
	private long id;
	
	@Column (name = "category_name")
	private String categoryName;
	
	@Column (name = "category_code")
	private String categoryCode;
}
