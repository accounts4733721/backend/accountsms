package com.adv.accountms.entity;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "bank_account")
@Data
@NoArgsConstructor
public class BankAccountEntity extends TableMaintenanceEntity {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO,generator="native")
	@GenericGenerator(name = "native",strategy = "native")
	private long id;

	private long accNum;

	private String bankName;
	
	private String ifscCode;
	
	private String branch;
	
	private String accHolderName;
	
	private String address;
}
