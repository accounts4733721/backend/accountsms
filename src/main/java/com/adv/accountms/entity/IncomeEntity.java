package com.adv.accountms.entity;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity (name = "income")
@Data
@NoArgsConstructor
public class IncomeEntity extends TableMaintenanceEntity {

	@Id
    //@GeneratedValue(strategy = GenerationType.UUID)
	//private UUID id;
	@GeneratedValue(strategy= GenerationType.AUTO,generator="native")
	@GenericGenerator(name = "native",strategy = "native")
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private CompanyEntity companyEntity;
	
	@ManyToOne
	@JoinColumn(name = "client_id")
	private ClientEntity clientEntity;
	
	@ManyToOne
	@JoinColumn(name = "bank_account_id")
	private BankAccountEntity bankAccountEntity;
		
	@ManyToOne
	@JoinColumn(name = "invoice_id")
	private InvoiceEntity invoiceEntity;

	private long amount;

	private int cgstPercent;
	
	private int sgstPercent;
	
	//@Temporal(TemporalType.TIMESTAMP)
	private String creditDate;
}
