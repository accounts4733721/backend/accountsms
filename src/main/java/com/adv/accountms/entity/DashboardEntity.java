package com.adv.accountms.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class DashboardEntity {

	@Id
	private long id;

	private long pending;
	
	private long income;
	
	private long expense;
	
	private String date;
}
