package com.adv.accountms.auditaware;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

@Service(value = "auditorAware")
public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.of("System");
		//SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
	}
}
