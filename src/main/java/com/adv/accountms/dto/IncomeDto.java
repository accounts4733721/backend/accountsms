package com.adv.accountms.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IncomeDto extends TableMaintenanceDto {
	
	private long id;
	
	private CompanyDto companyDto;
	
	private ClientDto clientDto;
	
	private BankAccountDto bankAccountDto;
		
	private InvoiceDto invoiceDto;

	private long amount;

	private int cgstPercent;
	
	private int sgstPercent;
	
	private String creditDate;
}
