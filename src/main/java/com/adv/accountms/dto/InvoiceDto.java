package com.adv.accountms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDto extends TableMaintenanceDto {

	private long id;
	
	private CompanyDto companyDto;
	
	private ClientDto clientDto;
	
	private BankAccountDto bankAccountDto;
	
	private String invoiceNumber;
	
	private long amount;
	
	private String date;
	
	private String status;
	
	private int cgstPercent;
	
	private int sgstPercent;

	private String expectedCreditDate;
}
