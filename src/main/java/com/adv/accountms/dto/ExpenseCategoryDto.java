package com.adv.accountms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExpenseCategoryDto extends TableMaintenanceDto {
	
	private long id;
	
	private String categoryName;
	
	private String categoryCode;
}
