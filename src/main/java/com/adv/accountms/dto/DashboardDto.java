package com.adv.accountms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DashboardDto {

	private String date;
	
	private long pending;

	private long income;

	private long expense;
	
	private long profitLoss;
}
