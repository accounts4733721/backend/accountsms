package com.adv.accountms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientDto extends TableMaintenanceDto {

	private long id;
	
	private String name;
	
	private String code;
}
