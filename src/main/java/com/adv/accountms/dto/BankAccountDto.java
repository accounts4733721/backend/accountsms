package com.adv.accountms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BankAccountDto extends TableMaintenanceDto {

	private long id;

	private long accNum;

	private String bankName;
	
	private String ifscCode;
	
	private String branch;
	
	private String accHolderName;
	
	private String address;
}
