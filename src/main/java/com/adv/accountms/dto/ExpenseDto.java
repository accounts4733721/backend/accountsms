package com.adv.accountms.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExpenseDto extends TableMaintenanceDto {
	
	private long id;
	
	private CompanyDto companyDto;
	
	private ExpenseCategoryDto expenseCategoryDto;

	private BankAccountDto bankAccountDto;
	
	private String expenseType;
	
	private long amount;
	
	private String status;
	
	private String debitDate;

	private int cgstPercent;
	
	private int sgstPercent;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public CompanyDto getCompanyDto() {
		return companyDto;
	}

	public void setCompanyDto(CompanyDto companyDto) {
		this.companyDto = companyDto;
	}

	public ExpenseCategoryDto getExpenseCategoryDto() {
		return expenseCategoryDto;
	}

	public void setExpenseCategoryDto(ExpenseCategoryDto expenseCategoryDto) {
		this.expenseCategoryDto = expenseCategoryDto;
	}

	public BankAccountDto getBankAccountDto() {
		return bankAccountDto;
	}

	public void setBankAccountDto(BankAccountDto bankAccountDto) {
		this.bankAccountDto = bankAccountDto;
	}

	public String getDebitDate() {
		return debitDate;
	}

	public void setDebitDate(String debitDate) {
		this.debitDate = debitDate;
	}

	public int getCgstPercent() {
		return cgstPercent;
	}

	public void setCgstPercent(int cgstPercent) {
		this.cgstPercent = cgstPercent;
	}

	public int getSgstPercent() {
		return sgstPercent;
	}

	public void setSgstPercent(int sgstPercent) {
		this.sgstPercent = sgstPercent;
	}
}
