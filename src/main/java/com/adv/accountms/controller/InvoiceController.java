package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.InvoiceDto;
import com.adv.accountms.service.InvoiceService;

@RestController
public class InvoiceController {

	@Autowired
	private InvoiceService invoiceService;
	
	@GetMapping(value = "/invoices")
	public ResponseEntity<List<InvoiceDto>> getAllInvoices(@RequestParam String startDate, @RequestParam String endDate) {
		return new ResponseEntity<List<InvoiceDto>>(invoiceService.getAllInvoices(startDate, endDate), HttpStatus.OK);
	}
	
	@PostMapping(value = "/invoices")
	public ResponseEntity<Void> createInvoice(@RequestBody InvoiceDto invoiceDto) {
		invoiceService.createInvoice(invoiceDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/invoices/{invoice_id}")
	public ResponseEntity<Void> updateInvoice(@PathVariable(value = "invoice_id") long invoiceId,
			@RequestBody InvoiceDto invoiceDto) {
		invoiceService.updateInvoice(invoiceId, invoiceDto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
