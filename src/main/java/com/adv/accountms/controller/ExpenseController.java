package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.ExpenseDto;
import com.adv.accountms.service.ExpenseService;

@RestController
public class ExpenseController {

	@Autowired
	private ExpenseService expensesService;

	@GetMapping("/expenses")
	public ResponseEntity<List<ExpenseDto>> getAllExpenses(@RequestParam String startDate, @RequestParam String endDate) {
		List<ExpenseDto> expensesDtos = expensesService.getAllExpenses(startDate, endDate);
		return new ResponseEntity<List<ExpenseDto>>(expensesDtos, HttpStatus.OK);
	}
	
	@PostMapping("/expenses")
	public ResponseEntity<Void> createExpenses(@RequestBody ExpenseDto expensesDto) {
		expensesService.createExpenses(expensesDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@PutMapping("/expenses/{expense_id}")
	public ResponseEntity<Void> updateExpenses(@PathVariable (value = "expense_id") long expenseId, @RequestBody ExpenseDto expensesDto) {
		expensesService.updateExpenses(expenseId, expensesDto);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
