package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.BankAccountDto;
import com.adv.accountms.service.BankAccountService;

@RestController
public class BankAccountController {

	@Autowired
	private BankAccountService bankAccountService;

	@GetMapping(value = "/bankaccounts")
	public ResponseEntity<List<BankAccountDto>> getAllBankAccounts() {
		return new ResponseEntity<List<BankAccountDto>>(
				bankAccountService.getAllBankAccounts(), HttpStatus.OK);
	}
}
