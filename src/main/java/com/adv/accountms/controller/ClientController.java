package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.ClientDto;
import com.adv.accountms.service.ClientService;

@RestController
public class ClientController {

	@Autowired
	private ClientService clientService;
	
	@GetMapping("/clients")
	public ResponseEntity<List<ClientDto>> getAllClients() {
		List<ClientDto> clientDaos = clientService.getAllClients();
		return new ResponseEntity<List<ClientDto>>(clientDaos, HttpStatus.OK);
	}
}
