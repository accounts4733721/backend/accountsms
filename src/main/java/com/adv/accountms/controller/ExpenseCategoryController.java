package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.ExpenseCategoryDto;
import com.adv.accountms.dto.ExpenseDto;
import com.adv.accountms.service.ExpenseCategoryService;
import com.adv.accountms.service.ExpenseService;

@RestController
public class ExpenseCategoryController {

	@Autowired
	private ExpenseCategoryService expenseCategoryService;
	
	@GetMapping("/expense-categories")
	public ResponseEntity<List<ExpenseCategoryDto>> getAllExpenseCategories() {
		List<ExpenseCategoryDto> expenseCategoryDtos = expenseCategoryService.getAllExpenseCategories();
		return new ResponseEntity<List<ExpenseCategoryDto>>(expenseCategoryDtos, HttpStatus.CREATED);
	}
}
