package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.IncomeDto;
import com.adv.accountms.service.IncomeService;

@RestController
public class IncomeController {

	@Autowired
	private IncomeService incomeService;
	
	@GetMapping(value = "/incomes")
	public ResponseEntity<List<IncomeDto>> getAllIncomes(@RequestParam String startDate, @RequestParam String endDate) {
		return new ResponseEntity<List<IncomeDto>>(incomeService.getAllIncomes(startDate, endDate), HttpStatus.OK);
	}
	
	@PostMapping(value = "/incomes")
	public ResponseEntity<Void> createIncome(@RequestBody IncomeDto incomeDto) {
		incomeService.createIncome(incomeDto);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
}
