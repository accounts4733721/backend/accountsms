package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.CompanyDto;
import com.adv.accountms.service.CompanyService;

@RestController
public class CompanyController {

	@Autowired
	private CompanyService companyService;
	
	@GetMapping("/companies")
	public ResponseEntity<List<CompanyDto>> getAllCompanies() {
		List<CompanyDto> companyDaos = companyService.getAllCompanies();
		return new ResponseEntity<List<CompanyDto>>(companyDaos, HttpStatus.OK);
	}
}
