package com.adv.accountms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adv.accountms.dto.DashboardDto;
import com.adv.accountms.service.DashboardService;

@RestController
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@GetMapping(value = "/dashboard-metrics")
	public ResponseEntity<List<DashboardDto>> getDashboardMetrics(@RequestParam String startDate,
			@RequestParam String endDate) {
		return new ResponseEntity<>(dashboardService.getDashboardMetrics(startDate, endDate), HttpStatus.OK);
	}
}
