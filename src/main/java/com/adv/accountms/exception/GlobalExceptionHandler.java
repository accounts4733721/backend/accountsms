package com.adv.accountms.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

//	@ExceptionHandler({ SortNotSupportedException.class, InvalidInputException.class })
//	public ResponseEntity<ErrorDto> handleSortInvalidInputEx(RuntimeException rtEx) {
//		String errorMessage = "";
//		String errorCode = "";
//		if (rtEx instanceof SortNotSupportedException) {
//			SortNotSupportedException snEx = (SortNotSupportedException) rtEx;
//			errorCode = snEx.getErrCode();
//			errorMessage = snEx.getErrMsg();
//		} else if (rtEx instanceof InvalidInputException) {
//			InvalidInputException inEx = (InvalidInputException) rtEx;
//			errorCode = inEx.getErrCode();
//			errorMessage = inEx.getErrMsg();
//		}
//		ErrorDto errorDTO = new ErrorDto(errorCode, errorMessage);
//		// logger.error(rtEx);
//		ResponseEntity<ErrorDto> responseEntity = new ResponseEntity<ErrorDto>(errorDTO, HttpStatus.BAD_REQUEST);
//		return responseEntity;
//	}
//
//	@ExceptionHandler(ResourceNotFoundException.class)
//	public ResponseEntity<ErrorDto> handleResourceNotFound(ResourceNotFoundException rnEx) {
//		String errorCode = rnEx.getErrCode();
//		String errorMessage = rnEx.getErrMsg();
//		ErrorDto errorDTO = new ErrorDto(errorCode, errorMessage);
//		// logger.error(rtEx);
//		ResponseEntity<ErrorDto> responseEntity = new ResponseEntity<ErrorDto>(errorDTO, HttpStatus.NOT_FOUND);
//		return responseEntity;
//	}
//
//	@ExceptionHandler(Exception.class)
//	public ResponseEntity<ErrorDto> handleResourceNotFound(Exception ex) {
//		String errorCode = "FMS003";
//		String errorMessage = ex.getMessage();
//		ErrorDto errorDTO = new ErrorDto(errorCode, errorMessage);
//		// logger.error(rtEx);
//		ResponseEntity<ErrorDto> responseEntity = new ResponseEntity<ErrorDto>(errorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
//		return responseEntity;
//	}
}
