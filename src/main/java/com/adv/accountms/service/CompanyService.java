package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.CompanyDto;

public interface CompanyService {

	public List<CompanyDto> getAllCompanies();
}
