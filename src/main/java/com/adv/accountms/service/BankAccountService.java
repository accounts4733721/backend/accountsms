package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.BankAccountDto;

public interface BankAccountService {

	public List<BankAccountDto> getAllBankAccounts();
}
