package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.ExpenseCategoryDto;

public interface ExpenseCategoryService {

	public List<ExpenseCategoryDto> getAllExpenseCategories();
}
