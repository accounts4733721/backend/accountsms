package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.IncomeDto;

public interface IncomeService {

	public List<IncomeDto> getAllIncomes(String startDate, String endDate);
	
	public void createIncome(IncomeDto incomeDto);
}
