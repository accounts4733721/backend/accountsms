package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.ExpenseDto;
import com.adv.accountms.entity.BankAccountEntity;
import com.adv.accountms.entity.CompanyEntity;
import com.adv.accountms.entity.ExpenseCategoryEntity;
import com.adv.accountms.entity.ExpenseEntity;
import com.adv.accountms.repository.ExpenseRepo;
import com.adv.accountms.service.ExpenseService;
import com.adv.accountms.util.EntityToDtoUtil;

@Service (value = "expensesServiceImpl")
public class ExpenseServiceImpl implements ExpenseService {

	@Autowired
	private ExpenseRepo expensesRepo;
	
	@Override
	public List<ExpenseDto> getAllExpenses(String startDate, String endDate) {
		List<ExpenseDto> expensesDtos = new ArrayList<>();
		List<ExpenseEntity> expensesEntities = expensesRepo.findByDebitDateBetween(startDate, endDate);
		expensesEntities.forEach(expenseEntity -> {
			ExpenseDto expensesDto = new ExpenseDto();
			expensesDto.setId(expenseEntity.getId());
			expensesDto.setAmount(expenseEntity.getAmount());
			expensesDto.setExpenseType(expenseEntity.getExpenseType());
			expensesDto.setStatus(expenseEntity.getStatus());
			expensesDto.setExpenseCategoryDto(EntityToDtoUtil.transformEntityToDto(expenseEntity.getExpenseCategoryEntity()));
			expensesDto.setCompanyDto(EntityToDtoUtil.transformEntityToDto(expenseEntity.getCompanyEntity()));
			expensesDto.setDebitDate(expenseEntity.getDebitDate());
			expensesDto.setBankAccountDto(EntityToDtoUtil.transformEntityToDto(expenseEntity.getBankAccountEntity()));
			expensesDto.setCgstPercent(expenseEntity.getCgstPercent());
			expensesDto.setSgstPercent(expenseEntity.getSgstPercent());
			EntityToDtoUtil.setTableMaintainceDetails(expensesDto, expenseEntity);
//			expensesDto.setCreatedBy(expenseEntity.getCreatedBy());
//			expensesDto.setModifiedBy(expenseEntity.getModifiedBy());
//			expensesDto.setCreatedDate(expenseEntity.getCreatedDate());
//			expensesDto.setModifiedDate(expenseEntity.getModifiedDate());
			
			expensesDtos.add(expensesDto);
		});
		return expensesDtos;
	}
	
	@Override
	public void createExpenses(ExpenseDto expensesDto) {
		ExpenseEntity expenseEntity = new ExpenseEntity();
		expenseEntity.setAmount(expensesDto.getAmount());
		expenseEntity.setExpenseType(expensesDto.getExpenseType());
		expenseEntity.setStatus(expensesDto.getStatus());
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(expensesDto.getCompanyDto().getId());
		expenseEntity.setCompanyEntity(companyEntity);
		ExpenseCategoryEntity expenseCategoryEntity = new ExpenseCategoryEntity();
		expenseCategoryEntity.setId(expensesDto.getExpenseCategoryDto().getId());
		expenseEntity.setExpenseCategoryEntity(expenseCategoryEntity);
		BankAccountEntity bankAccountEntity = new BankAccountEntity();
		bankAccountEntity.setId(expensesDto.getBankAccountDto().getId());
		expenseEntity.setBankAccountEntity(bankAccountEntity);
		expenseEntity.setCgstPercent(expensesDto.getCgstPercent());
		expenseEntity.setSgstPercent(expensesDto.getSgstPercent());
		expenseEntity.setDebitDate(expensesDto.getDebitDate());
		expensesRepo.saveAndFlush(expenseEntity);
	}
	
	@Override
	public void updateExpenses(long expenseId, ExpenseDto expensesDto) {
		ExpenseEntity expenseEntity = new ExpenseEntity();
		expenseEntity.setId(expenseId);
		expenseEntity.setAmount(expensesDto.getAmount());
		expenseEntity.setExpenseType(expensesDto.getExpenseType());
		expenseEntity.setStatus(expensesDto.getStatus());
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(expensesDto.getCompanyDto().getId());
		expenseEntity.setCompanyEntity(companyEntity);
		ExpenseCategoryEntity expenseCategoryEntity = new ExpenseCategoryEntity();
		expenseCategoryEntity.setId(expensesDto.getExpenseCategoryDto().getId());
		expenseEntity.setExpenseCategoryEntity(expenseCategoryEntity);
		BankAccountEntity bankAccountEntity = new BankAccountEntity();
		bankAccountEntity.setId(expensesDto.getBankAccountDto().getId());
		expenseEntity.setBankAccountEntity(bankAccountEntity);
		expenseEntity.setCgstPercent(expensesDto.getCgstPercent());
		expenseEntity.setSgstPercent(expensesDto.getSgstPercent());
		expenseEntity.setDebitDate(expensesDto.getDebitDate());
		expensesRepo.saveAndFlush(expenseEntity);
	}
}
