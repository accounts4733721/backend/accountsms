package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.BankAccountDto;
import com.adv.accountms.entity.BankAccountEntity;
import com.adv.accountms.repository.BankAccountRepo;
import com.adv.accountms.service.BankAccountService;
import com.adv.accountms.util.EntityToDtoUtil;

@Service(value = "bankAccountServiceImpl")
public class BankAccountServiceImpl implements BankAccountService {

	@Autowired
	private BankAccountRepo bankAccountRepo;
	
	@Override
	public List<BankAccountDto> getAllBankAccounts() {
		List<BankAccountDto> bankAccountDtos = new ArrayList<>();
		List<BankAccountEntity> bankAccountEntities = bankAccountRepo.findAll();
		bankAccountEntities.forEach(e -> {
			bankAccountDtos.add(EntityToDtoUtil.transformEntityToDto(e));
		});
		return bankAccountDtos;
	}

}
