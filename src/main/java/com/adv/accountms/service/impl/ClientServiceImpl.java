package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.ClientDto;
import com.adv.accountms.entity.ClientEntity;
import com.adv.accountms.repository.ClientRepo;
import com.adv.accountms.service.ClientService;

@Service(value = "clientServiceImpl")
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepo clientRepo;
	
	@Override
	public List<ClientDto> getAllClients() {
		List<ClientEntity> companyEntities = clientRepo.findAll();
		List<ClientDto> clientDaos = new ArrayList<>();
		companyEntities.forEach(e -> {
			ClientDto clientDao = new ClientDto(e.getId(), e.getName(), e.getCode());
			clientDaos.add(clientDao);
		});
		return clientDaos;
	}
}
