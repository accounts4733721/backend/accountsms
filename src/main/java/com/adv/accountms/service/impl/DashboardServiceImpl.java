package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.DashboardDto;
import com.adv.accountms.entity.DashboardEntity;
import com.adv.accountms.repository.DashboardRepo;
import com.adv.accountms.service.DashboardService;

@Service(value = "dashboardServiceImpl")
public class DashboardServiceImpl implements DashboardService {

	@Autowired
	private DashboardRepo dashboardRepo;
	
	@Override
	public List<DashboardDto> getDashboardMetrics(String startDate, String endDate) {
		List<DashboardEntity> dashboardEntities = dashboardRepo.findDashboardMetrics(startDate, endDate);
		List<DashboardDto> dashboardDtos = new ArrayList<>();
		dashboardEntities.forEach(e -> {
			dashboardDtos.add(new DashboardDto(e.getDate(), e.getPending(), e.getIncome(), e.getExpense(),
					(e.getIncome() - e.getExpense())));
		});
		return dashboardDtos;
	}
}