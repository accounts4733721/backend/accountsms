package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.ExpenseCategoryDto;
import com.adv.accountms.entity.ExpenseCategoryEntity;
import com.adv.accountms.repository.ExpenseCategoryRepo;
import com.adv.accountms.service.ExpenseCategoryService;

@Service (value = "expenseCategoryServiceImpl")
public class ExpenseCategoryServiceImpl implements ExpenseCategoryService {

	@Autowired
	private ExpenseCategoryRepo expenseCategoryRepo;
	
	@Override
	public List<ExpenseCategoryDto> getAllExpenseCategories() {
		List<ExpenseCategoryDto> expenseCategoryDtos = new ArrayList<>();
		List<ExpenseCategoryEntity> expensesEntities = expenseCategoryRepo.findAll();
		expensesEntities.forEach(e -> {
			ExpenseCategoryDto expenseCategoryDto = new ExpenseCategoryDto(e.getId(), e.getCategoryName(),
					e.getCategoryCode());
			expenseCategoryDtos.add(expenseCategoryDto);
		});
		return expenseCategoryDtos;
	}
}
