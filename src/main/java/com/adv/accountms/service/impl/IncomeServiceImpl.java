package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.IncomeDto;
import com.adv.accountms.entity.BankAccountEntity;
import com.adv.accountms.entity.ClientEntity;
import com.adv.accountms.entity.CompanyEntity;
import com.adv.accountms.entity.IncomeEntity;
import com.adv.accountms.entity.InvoiceEntity;
import com.adv.accountms.repository.IncomeRepo;
import com.adv.accountms.service.IncomeService;
import com.adv.accountms.util.EntityToDtoUtil;

@Service(value = "incomeServiceImpl")
public class IncomeServiceImpl implements IncomeService {

	@Autowired
	private IncomeRepo incomeRepo;

	@Override
	public List<IncomeDto> getAllIncomes(String startDate, String endDate) {
		List<IncomeEntity> incomeEntities = incomeRepo.findByCreditDateBetween(startDate, endDate);
		List<IncomeDto> incomeDtos = new ArrayList<>();
		incomeEntities.forEach(incomeEntity -> {
			IncomeDto incomeDto = new IncomeDto();
			incomeDto.setId(incomeEntity.getId());
			incomeDto.setCompanyDto(EntityToDtoUtil.transformEntityToDto(incomeEntity.getCompanyEntity()));
			incomeDto.setClientDto(EntityToDtoUtil.transformEntityToDto(incomeEntity.getClientEntity()));
			incomeDto.setBankAccountDto(EntityToDtoUtil.transformEntityToDto(incomeEntity.getBankAccountEntity()));
			incomeDto.setInvoiceDto(EntityToDtoUtil.transformEntityToDto(incomeEntity.getInvoiceEntity()));
			incomeDto.setCgstPercent(incomeEntity.getCgstPercent());
			incomeDto.setSgstPercent(incomeEntity.getSgstPercent());
			incomeDto.setCreditDate(incomeEntity.getCreditDate());
			EntityToDtoUtil.setTableMaintainceDetails(incomeDto, incomeEntity);
			incomeDtos.add(incomeDto);
		});
		return incomeDtos;
	}

	@Override
	public void createIncome(IncomeDto incomeDto) {
		IncomeEntity incomeEntity = new IncomeEntity();
		incomeEntity.setAmount(incomeDto.getAmount());
		InvoiceEntity invoiceEntity = new InvoiceEntity();
		invoiceEntity.setId(incomeDto.getInvoiceDto().getId());
		incomeEntity.setInvoiceEntity(invoiceEntity);
		ClientEntity clientEntity = new ClientEntity();
		clientEntity.setId(incomeDto.getClientDto().getId());
		incomeEntity.setClientEntity(clientEntity);
		CompanyEntity companyEntity = new CompanyEntity();
		companyEntity.setId(incomeDto.getCompanyDto().getId());
		incomeEntity.setCompanyEntity(companyEntity);
		BankAccountEntity bankAccountEntity = new BankAccountEntity();
		bankAccountEntity.setId(incomeDto.getBankAccountDto().getId());
		incomeEntity.setBankAccountEntity(bankAccountEntity);
		incomeEntity.setCgstPercent(incomeDto.getCgstPercent());
		incomeEntity.setSgstPercent(incomeDto.getSgstPercent());
		incomeEntity.setCreditDate(incomeDto.getCreditDate());
		incomeRepo.saveAndFlush(incomeEntity);
	}
}
