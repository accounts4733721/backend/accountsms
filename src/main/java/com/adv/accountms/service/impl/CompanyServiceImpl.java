package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.accountms.dto.CompanyDto;
import com.adv.accountms.entity.CompanyEntity;
import com.adv.accountms.repository.CompanyRepo;
import com.adv.accountms.service.CompanyService;

@Service(value = "companyServiceImpl")
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepo companyRepo;
	
	@Override
	public List<CompanyDto> getAllCompanies() {
		List<CompanyEntity> companyEntities = companyRepo.findAll();
		List<CompanyDto> companyDaos = new ArrayList<>();
		companyEntities.forEach(e -> {
			CompanyDto companyDao = new CompanyDto(e.getId(), e.getName(), e.getCode());
			companyDaos.add(companyDao);
		});
		return companyDaos;
	}
}
