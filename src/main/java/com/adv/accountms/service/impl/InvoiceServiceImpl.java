package com.adv.accountms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.adv.accountms.dto.IncomeDto;
import com.adv.accountms.dto.InvoiceDto;
import com.adv.accountms.entity.BankAccountEntity;
import com.adv.accountms.entity.ClientEntity;
import com.adv.accountms.entity.CompanyEntity;
import com.adv.accountms.entity.InvoiceEntity;
import com.adv.accountms.repository.InvoiceRepo;
import com.adv.accountms.service.InvoiceService;
import com.adv.accountms.util.EntityToDtoUtil;

@Service(value = "invoiceServiceImpl")
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceRepo invoiceRepo;

	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public List<InvoiceDto> getAllInvoices(String startDate, String endDate) {
		List<InvoiceEntity> invoiceEntities = invoiceRepo.findByDateBetween(startDate, endDate);
		List<InvoiceDto> invoiceDtos = new ArrayList<>();
		invoiceEntities.forEach(e -> {
			InvoiceDto invoiceDto = new InvoiceDto();
			invoiceDto.setId(e.getId());
			invoiceDto.setCompanyDto(EntityToDtoUtil.transformEntityToDto(e.getCompanyEntity()));
			invoiceDto.setClientDto(EntityToDtoUtil.transformEntityToDto(e.getClientEntity()));
			invoiceDto.setBankAccountDto(EntityToDtoUtil.transformEntityToDto(e.getBankAccountEntity()));
			invoiceDto.setInvoiceNumber(e.getInvoiceNumber());
			invoiceDto.setAmount(e.getAmount());
			invoiceDto.setDate(e.getDate());
			invoiceDto.setStatus(e.getStatus());
			invoiceDto.setCgstPercent(e.getCgstPercent());
			invoiceDto.setSgstPercent(e.getSgstPercent());
			invoiceDto.setExpectedCreditDate(e.getExpectedCreditDate());
			invoiceDtos.add(invoiceDto);
		});
		return invoiceDtos;
	}

	@Override
	public void createInvoice(InvoiceDto invoiceDto) {
		InvoiceEntity invoiceEntity = new InvoiceEntity();
		invoiceEntity.setCompanyEntity(new CompanyEntity() {{setId(invoiceDto.getCompanyDto().getId());}});
		invoiceEntity.setClientEntity(new ClientEntity() {{setId(invoiceDto.getClientDto().getId());}});
		invoiceEntity.setBankAccountEntity(new BankAccountEntity() {{setId(invoiceDto.getBankAccountDto().getId());}});
		invoiceEntity.setInvoiceNumber(invoiceDto.getInvoiceNumber());
		invoiceEntity.setAmount(invoiceDto.getAmount());
		invoiceEntity.setDate(invoiceDto.getDate());
		invoiceEntity.setStatus(invoiceDto.getStatus());
		invoiceEntity.setCgstPercent(invoiceDto.getCgstPercent());
		invoiceEntity.setSgstPercent(invoiceDto.getSgstPercent());
		invoiceEntity.setExpectedCreditDate(invoiceDto.getExpectedCreditDate());
		invoiceRepo.saveAndFlush(invoiceEntity);
	}
	
	@Override
	public void updateInvoice(long invoiceId, InvoiceDto invoiceDto) {
		InvoiceEntity invoiceEntity = new InvoiceEntity();	
		invoiceEntity.setId(invoiceId);
		invoiceEntity.setCompanyEntity(new CompanyEntity() {{setId(invoiceDto.getCompanyDto().getId());}});
		invoiceEntity.setClientEntity(new ClientEntity() {{setId(invoiceDto.getClientDto().getId());}});
		invoiceEntity.setBankAccountEntity(new BankAccountEntity() {{setId(invoiceDto.getBankAccountDto().getId());}});
		invoiceEntity.setInvoiceNumber(invoiceDto.getInvoiceNumber());
		invoiceEntity.setAmount(invoiceDto.getAmount());
		invoiceEntity.setDate(invoiceDto.getDate());
		invoiceEntity.setStatus(invoiceDto.getStatus());
		invoiceEntity.setCgstPercent(invoiceDto.getCgstPercent());
		invoiceEntity.setSgstPercent(invoiceDto.getSgstPercent());
		invoiceEntity.setExpectedCreditDate(invoiceDto.getExpectedCreditDate());
		invoiceRepo.saveAndFlush(invoiceEntity);
		
		if (invoiceDto.getStatus().equalsIgnoreCase("approved")) {
			IncomeDto incomeDto = new IncomeDto();
			incomeDto.setCompanyDto(invoiceDto.getCompanyDto());
			incomeDto.setClientDto(invoiceDto.getClientDto());
			incomeDto.setBankAccountDto(invoiceDto.getBankAccountDto());
			incomeDto.setInvoiceDto(new InvoiceDto() {{setId(invoiceId);}});
			incomeDto.setAmount(invoiceDto.getAmount());
			incomeDto.setCgstPercent(invoiceDto.getCgstPercent());
			incomeDto.setSgstPercent(invoiceDto.getSgstPercent());
			incomeDto.setCreditDate(invoiceDto.getExpectedCreditDate());
			ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8182/accountms/incomes", incomeDto, String.class);
			int statusCode = responseEntity.getStatusCode().value();
			System.out.println("statusCode="+statusCode);
		}
	}
}
