package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.ExpenseDto;

public interface ExpenseService {

	public List<ExpenseDto> getAllExpenses(String startDate, String endDate);
	
	public void createExpenses(ExpenseDto expensesDto);
	
	public void updateExpenses(long expenseId, ExpenseDto expensesDto);
}
