package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.ClientDto;

public interface ClientService {

	public List<ClientDto> getAllClients();
}
