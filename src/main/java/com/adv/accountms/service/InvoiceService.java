package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.InvoiceDto;

public interface InvoiceService {

	public List<InvoiceDto> getAllInvoices(String startDate, String endDate);
	
	public void createInvoice(InvoiceDto invoiceDto);
	
	public void updateInvoice(long invoiceId, InvoiceDto invoiceDto);
}
