package com.adv.accountms.service;

import java.util.List;

import com.adv.accountms.dto.DashboardDto;

public interface DashboardService {

	public List<DashboardDto> getDashboardMetrics(String startDate, String endDate);
}
