package com.adv.accountms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adv.accountms.entity.DashboardEntity;

@Repository
public interface DashboardRepo extends JpaRepository<DashboardEntity, Long> {
	
	@Query(value = "select ROW_NUMBER() OVER(ORDER BY sub_date desc) as id, IFNULL(max(case when type ='pending' then amount end), 0) as pending, IFNULL(max(case when type ='income' then amount end), 0) as income, IFNULL(max(case when type ='expense' then amount end), 0) as expense, sub_date as date from (select sum(amount) as amount, substring(date, 1, 7) as sub_date, 'pending' as type from invoice where date between ?1 and ?2 and status='pending' group by sub_date union select sum(amount) as amount, substring(credit_date, 1, 7) as sub_date, 'income' as type from income where credit_date between ?1 and ?2 group by sub_date union select sum(amount) as amount, substring(debit_date, 1, 7) as sub_date, 'expense' as type from expense where debit_date between ?1 and ?2 group by sub_date) as t1 group by sub_date", nativeQuery = true)
	public List<DashboardEntity> findDashboardMetrics(String startDate, String endDate);
}