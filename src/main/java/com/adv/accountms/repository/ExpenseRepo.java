package com.adv.accountms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adv.accountms.entity.ExpenseEntity;

//public interface ExpenseRepo extends JpaRepository<ExpensesEntity, UUID> {
public interface ExpenseRepo extends JpaRepository<ExpenseEntity, Long> {

	public List<ExpenseEntity> findByDebitDateBetween(String startDate, String endDate);
}
