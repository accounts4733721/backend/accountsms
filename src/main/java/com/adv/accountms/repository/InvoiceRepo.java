package com.adv.accountms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adv.accountms.entity.InvoiceEntity;

@Repository
public interface InvoiceRepo extends JpaRepository<InvoiceEntity, Long> {

	public List<InvoiceEntity> findByDateBetween(String startDate, String endDate);
}
