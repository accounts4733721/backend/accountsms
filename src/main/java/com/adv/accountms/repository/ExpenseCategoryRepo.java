package com.adv.accountms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adv.accountms.entity.ExpenseCategoryEntity;

//public interface ExpensesRepo extends JpaRepository<ExpensesEntity, UUID> {
public interface ExpenseCategoryRepo extends JpaRepository<ExpenseCategoryEntity, Long> {

}
