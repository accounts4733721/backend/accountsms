package com.adv.accountms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adv.accountms.entity.BankAccountEntity;

@Repository
public interface BankAccountRepo extends JpaRepository<BankAccountEntity, Long> {

}
