package com.adv.accountms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adv.accountms.entity.IncomeEntity;

@Repository
public interface IncomeRepo extends JpaRepository<IncomeEntity, Long> {

	public List<IncomeEntity> findByCreditDateBetween(String startDate, String endDate);
}
