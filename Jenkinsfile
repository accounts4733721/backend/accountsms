pipeline {
    agent { label 'jenkins_slave' }
	environment {
		def buildNumber = "${env.BUILD_NUMBER}"
	}
    stages {		
        stage('SCM Checkout') {
            steps {
                git branch: 'main', url:'https://gitlab.com/product1554052/accounts4733721/backend/accountms.git'
            }
        }

        stage('Compile') {
            steps {
                sh 'mvn clean install -DskipTests'
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    sh "sudo docker build -t rkexploreaws/accountsms:${buildNumber} ."
                }
            }
        }

        stage('Docker Image Push') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'docker_pass', variable: 'DOCKER_PASSWORD')]) {
                        sh 'echo $DOCKER_PASSWORD | sudo docker login -u rkexploreaws --password-stdin'
                        sh "sudo docker push rkexploreaws/accountsms:${buildNumber}"
                    }
                }
            }
        }
		
		stage('Connect to Target Ec2, pull & run the docker image') {
            steps {
				script {
					sh """
						ssh -i /opt/jenkins/jenkins-server-comm-keypair.pem jenkins@${target_ip} "sudo docker pull rkexploreaws/accountsms:${buildNumber}"
						ssh -i /opt/jenkins/jenkins-server-comm-keypair.pem jenkins@${target_ip} "sudo docker stop ${JOB_BASE_NAME} || true && sudo docker rm ${JOB_BASE_NAME} || true"
						ssh -i /opt/jenkins/jenkins-server-comm-keypair.pem jenkins@${target_ip} "sudo docker run -d --name ${JOB_BASE_NAME} -it -p 8182:8182  -e environment=${DEPLOY_ENV} rkexploreaws/accountsms:${buildNumber}"
					"""
				}
            }
        }
		
		stage('remove the docker image') {
            steps {
				script {
					try {
						def oldBuildNum = (buildNumber as int) - 1
						sh """
							ssh -i /opt/jenkins/jenkins-server-comm-keypair.pem jenkins@${target_ip} "sudo docker rmi rkexploreaws/accountsms:${oldBuildNum}"
						"""
					} catch (err) {
						echo "Failed: ${err}"
					}
				}
            }
        }
    }
}
