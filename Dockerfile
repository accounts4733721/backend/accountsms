FROM openjdk:17-alpine
COPY /target/accountms-0.0.1.jar app.jar
#ENTRYPOINT ["sh", "-c", "java -jar app.jar -Denv={environment}"]
ENTRYPOINT ["sh", "-c", "java -jar app.jar -Denv=dev"]